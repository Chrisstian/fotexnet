Telepítési útmutató

A program symfony keretrendszert használ.
A program php 8.2-es verziót igényel.
A symfony telepítéséhez composer-re lesz szükség.
Composer letöltése: https://getcomposer.org/download/

Amennyiben a megfelelő verziójú php és composer rendelkezésre állnak, lehet telepíteni a symfony-t.
"composer install" paranccsal települ a symfony, a composer.json alapján.

Telepítés után, be kell állítani, a környezeti változókat.
A környezeti változók, az .env file-ban találhatók. Erről a file-ról, kell készíteni, egy másolatot, .env.local néven.
Az új file-ban (.env.local), a következő értékeket kell beállítani:
    - a nem kikommentezett DATABASE_URL változót, ami mysql alapú (minta: mysql://userName:Password@127.0.0.1:3306/databaseName)
    - PORT_PROVIDER, ami a port.hu elérési útvonalát tartalmazza: "https://port.hu"

A miután a két változó beállításra került, kell készíteni, egy php kiterjesztésű környezeti file-t is. Ezt a composer megcsinálja helyettünk.
"composer dump-env" paranccsal elkészül, az .env.local.php file.
Ezzel meg is vagyunk, a környezeti változók beállításaival.

A telepítés könnyítése miatt, nem ORM-et használtam, adatbázis kezelésre, hanem natív PDO-t.
A létrehozott adatbázisba, importálni szükséges, a fotexnet.sql file-t.
(4 táblát tartalmaz, amiből 2 törzsadatokat tartalmaz.)



Program futtatása

Összesen 2 végpontot hoztam létre, amelyek a következők: /list/menu és /update/menu
A két végpont GET-es és POST-os http protokollal is elérhetőek.
A végpontokat natívan, így lehet elérni: http://ipcím/projectMappaNeve/public/index.php/vegpont

Linuxon lehet port használatával elérést biztosítani, a következő paranccsal: php -S 0.0.0.0:1300 -t public
Ebben az esetben, így lehet elérni a végpontokat: http://ipcim:1300/vegpont


Remélem gond és hiba nélkül sikerül működésre bírni a rendszer.
Amennyiben probléma merül fel, kérem keressenek bizalommal és elnézésüket kérem.