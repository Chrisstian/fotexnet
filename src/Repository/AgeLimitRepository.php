<?php

namespace App\Repository;

use App\Constant\Project;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use PDO;

/**
 * Class AgeLimitRepository
 */
class AgeLimitRepository {

    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Korhatár ID lekérdezése
     *
     * @param int $ageLimit
     * @return int
     * @throws Exception
     */
    public function searchAgeLimitId(int $ageLimit): int {

        $query = "SELECT id
                  FROM age_limit
                  WHERE value = :value
                  AND is_active = :is_active";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':value', $ageLimit, PDO::PARAM_STR);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchOne();
    }
}