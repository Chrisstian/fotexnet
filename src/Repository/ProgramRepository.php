<?php

namespace App\Repository;

use App\Constant\Project;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use PDO;

/**
 * Class ProgramRepository
 */
class ProgramRepository
{

    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Új műsor rögzítése
     *
     * @param array $programData
     * @return void
     * @throws Exception
     */
    public function createProgram(array $programData): void {

        $query = "INSERT INTO program
                  SET channel_id = :channel_id,
                      age_id = :age_id,
                      title = :title,
                      start_calendar_id = :start_calendar_id,
                      start = :start,
                      finish_calendar_id = :finish_calendar_id,
                      finish = :finish,
                      description = :description,
                      is_active = :is_active,
                      created_at = NOW()";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':channel_id', $programData['channel_id'], PDO::PARAM_INT);
        $stmt->bindValue(':age_id', $programData['age_id'], PDO::PARAM_INT);
        $stmt->bindValue(':title', $programData['title'], PDO::PARAM_STR);
        $stmt->bindValue(':start_calendar_id', $programData['start_calendar_id'], PDO::PARAM_INT);
        $stmt->bindValue(':start', $programData['start'], PDO::PARAM_STR);
        $stmt->bindValue(':finish_calendar_id', $programData['finish_calendar_id'], PDO::PARAM_INT);
        $stmt->bindValue(':finish', $programData['finish'], PDO::PARAM_STR);
        $stmt->bindValue(':description', $programData['description'], PDO::PARAM_STR);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        $stmt->executeQuery();
    }

    /**
     * Műsor létezésének ellenőrzése
     *
     * @param int $channelId
     * @param int $calendarId
     * @param string $startTime
     * @param string $title
     * @return mixed
     * @throws Exception
     */
    public function checkProgramExist(int    $channelId,
                                      int    $calendarId,
                                      string $startTime,
                                      string $title): mixed {

        $query = "SELECT 1 AS found
                  FROM program
                  WHERE channel_id = :channel_id
                  AND start_calendar_id = :start_calendar_id
                  AND start = :start
                  AND title = :title
                  AND is_active = :is_active";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':channel_id', $channelId, PDO::PARAM_INT);
        $stmt->bindValue(':start_calendar_id', $calendarId, PDO::PARAM_INT);
        $stmt->bindValue(':start', $startTime, PDO::PARAM_STR);
        $stmt->bindValue(':title', $title, PDO::PARAM_STR);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchOne();
    }

    /**
     * Műsor kilistázása
     *
     * @param array $formData
     * @return array
     * @throws Exception
     */
    public function listPrograms(array $formData): array {

        $query = "SELECT c.id AS channel_id, c.name AS channel_name, CONCAT(cd.date, ' ', p.start) AS start_time, p.title, p.description, al.value AS age_limit
                  FROM program AS p
                  INNER JOIN channel AS c ON (p.channel_id = c.id AND c.is_active = :is_active)
                  INNER JOIN age_limit AS al ON (p.age_id = al.id AND al.is_active = :is_active)
                  INNER JOIN calendar_day AS cd ON (p.start_calendar_id = cd.id)
                  WHERE c.id IN (".$formData['channelList'].")
                  AND p.start_calendar_id IN (".$formData['calendarId'].")
                  ORDER BY c.id, p.start_calendar_id, p.start ASC";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchAllAssociative();
    }

}