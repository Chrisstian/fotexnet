<?php

namespace App\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use PDO;

/**
 * Class CalendarRepository
 */
class CalendarRepository {

    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Dátum ID lekérdezése
     *
     * @param string $date
     * @return int
     * @throws Exception
     */
    public function searchDateId(string $date): int {

        $query = "SELECT id
                  FROM calendar_day
                  WHERE date = :date";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':date', $date, PDO::PARAM_STR);
        return $stmt->executeQuery()->fetchOne();
    }
}