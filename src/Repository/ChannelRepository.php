<?php

namespace App\Repository;

use App\Constant\Project;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use PDO;

/**
 * Class ChannelRepository
 */
class ChannelRepository
{

    /**
     * @var Connection
     */
    private Connection $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Csatorna létezésének ellenőzrése
     *
     * @param string $channelId
     * @return mixed
     * @throws Exception
     */
    public function checkChannelExist(string $channelId): mixed {

        $query = "SELECT 1 AS found
                  FROM channel
                  WHERE channel_id = :channel_id
                  AND is_active = :is_active";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':channel_id', $channelId, PDO::PARAM_STR);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchOne();
    }

    /**
     * Új csatorna rögzítése
     *
     * @param array $channelData
     * @return void
     * @throws Exception
     */
    public function createChannel(array $channelData): void {

        $query = "INSERT INTO channel
                  SET channel_id = :channel_id,
                      name = :name,
                      link = :link,
                      is_active = :is_active,
                      created_at = NOW()";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':channel_id', $channelData['id'], PDO::PARAM_STR);
        $stmt->bindValue(':name', $channelData['name'], PDO::PARAM_STR);
        $stmt->bindValue(':link', $channelData['link'], PDO::PARAM_STR);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        $stmt->executeQuery();
    }

    /**
     * Csatorna tábla rekordjainak megszámolása
     *
     * @return int
     * @throws Exception
     */
    public function countChannel(): int {

        $query = "SELECT COUNT(id)
                  FROM channel
                  WHERE is_active = :is_active";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchOne();
    }

    /**
     * Csatornák kilistázása
     *
     * @return array
     * @throws Exception
     */
    public function listChannels(): array {

        $query = "SELECT *
                  FROM channel
                  WHERE is_active = :is_active
                  ORDER BY id ASC";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * Csatornák kilistázása
     *
     * @param string $channels
     * @return array
     * @throws Exception
     */
    public function searchChannelById(string $channels): array {

        $query = "SELECT *
                  FROM channel
                  WHERE is_active = :is_active
                  AND id IN ($channels)
                  ORDER BY id ASC";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchAllAssociative();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function listProgramDates(): array {

        $query = "SELECT cd.date
                  FROM program AS p
                  INNER JOIN calendar_day AS cd ON (p.start_calendar_id = cd.id)
                  WHERE p.is_active = :is_active
                  GROUP BY cd.id";

        $stmt = $this->connection->prepare($query);
        $stmt->bindValue(':is_active', Project::IS_ACTIVE, PDO::PARAM_INT);
        return $stmt->executeQuery()->fetchAllAssociative();
    }
}