<?php

namespace App\Constant;

/**
 * Class Project
 */
class Project {

    public const CHANNEL_UPDATE_ERROR_MSG = 'Sikertelen csatorna lista frissítés';
    public const CHANNEL_UPDATE_SUCCESS_MSG = 'Sikeres csatorna lista frissítés';
    public const PROGRAM_UPDATE_ERROR_MSG = 'Sikertelen műsor lista frissítés';
    public const PROGRAM_UPDATE_EMPTY_ERROR_MSG = 'Nem áll rendelkezésre adat';
    public const CHANNEL_FORM_VALIDATION_ERROR_MSG = 'Paraméter hiba';
    public const NOT_RESULT_MSG = 'Nincs találat';

    public const IS_ACTIVE = 1;
    public const DEFAULT_CHANNELS = '1,2,3,4,5';
}