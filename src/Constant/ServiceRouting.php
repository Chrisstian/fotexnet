<?php

namespace App\Constant;

/**
 * Class ServiceRouting
 */
class ServiceRouting {

    public const PORT_CHANNEL_ROUTE = '/tvapi/init-new';
    public const PORT_PROGRAM_ROUTE = '/tvapi';
}