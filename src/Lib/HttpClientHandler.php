<?php

namespace App\Lib;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class HttpClientHandler
 */
class HttpClientHandler
{
    public function __construct(private HttpClientInterface $client) {

    }

    /**
     * Http client hívás elvégzése
     *
     * @param string $method
     * @param string $endpoint
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function callClient(string $method,
                               string $endpoint): array {

        $request = $this->client->request(
            $method,
            $endpoint
        );

        $response['statusCode'] = $request->getStatusCode();
        $response['contentType'] = $request->getHeaders()['content-type'][0];
        $response['content'] = $request->getContent();
        $response['contentArray'] = $request->toArray();

        return $response;
    }
}