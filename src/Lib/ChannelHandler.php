<?php

namespace App\Lib;

use App\Repository\AgeLimitRepository;
use App\Repository\CalendarRepository;
use App\Repository\ProgramRepository;
use Exception;
use App\Constant\Project;
use App\Constant\ServiceRouting;
use App\Repository\ChannelRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ChannelHandler
 */
class ChannelHandler {

    /**
     * Csatorna lista frissítése
     *
     * @param HttpClientHandler $httpClientHandler
     * @param ChannelRepository $channelRepository
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function updateChannel(HttpClientHandler $httpClientHandler,
                                  ChannelRepository $channelRepository): void {

        $channelListResponse = $httpClientHandler->callClient('GET', $_ENV['PORT_PROVIDER'] . ServiceRouting::PORT_CHANNEL_ROUTE);
        if($channelListResponse['statusCode'] === Response::HTTP_OK) {
            foreach($channelListResponse['contentArray']['channels'] as $channel) {
                $channelExist = $channelRepository->checkChannelExist($channel['id']);
                if(!$channelExist) {
                    $channelRepository->createChannel($channel);
                }
            }
        } else {
            throw new Exception(Project::CHANNEL_UPDATE_ERROR_MSG, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * Form validálás
     *
     * @param array $channelName
     * @param string $startDate
     * @param CalendarRepository $calendarRepository
     * @return array
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function formValidation(array              $channelName,
                                   string             $startDate,
                                   CalendarRepository $calendarRepository): array {

        if(count($channelName) > 0) {
            if($this->channelFormDataValidation($channelName['channelName']) === false) {
                throw new Exception(Project::CHANNEL_FORM_VALIDATION_ERROR_MSG, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $channelList = implode(',', $channelName['channelName']);
        } else {
            $channelList = Project::DEFAULT_CHANNELS;
        }

        if($startDate !== "") {
            $calendarId = $calendarRepository->searchDateId($startDate);
        } else {
            $today = date('Y-m-d');
            $calendarId = $calendarRepository->searchDateId($today);
        }

        return array(
            'channelList' => $channelList,
            'calendarId' => $calendarId
        );
    }

    /**
     * Csatorna lista validálása
     *
     * @param array $channels
     * @return bool
     */
    private function channelFormDataValidation(array $channels): bool {

        foreach($channels as $channel) {
            if(!is_numeric($channel)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Program firssítése
     *
     * @param array $channels
     * @param string $date
     * @param HttpClientHandler $httpClientHandler
     * @param ProgramRepository $programRepository
     * @param AgeLimitRepository $ageLimitRepository
     * @param CalendarRepository $calendarRepository
     * @return void
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function programUpdating(array              $channels,
                                    string             $date,
                                    HttpClientHandler  $httpClientHandler,
                                    ProgramRepository  $programRepository,
                                    AgeLimitRepository $ageLimitRepository,
                                    CalendarRepository $calendarRepository): void {

        foreach($channels as $channel) {
            $programResponse = $httpClientHandler->callClient('GET', $_ENV['PORT_PROVIDER'] . ServiceRouting::PORT_PROGRAM_ROUTE . '?channel_id[]='.$channel['channel_id'].'&i_datetime_from='.$date.'&i_datetime_to='.$date);
            if($programResponse['statusCode'] === Response::HTTP_OK) {
                $contentArrayKey = array_key_first($programResponse['contentArray']);

                if(empty($programResponse['contentArray'][$contentArrayKey]['channels'])) {
                    throw new Exception(Project::PROGRAM_UPDATE_EMPTY_ERROR_MSG, Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                $programs = $programResponse['contentArray'][$contentArrayKey]['channels'][0]['programs'];
                $this->prepareSaveProgram($programs, $channel['id'], $programRepository, $ageLimitRepository, $calendarRepository);
            } else {
                throw new Exception(Project::PROGRAM_UPDATE_ERROR_MSG, Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        }
    }

    /**
     * Program elmentés előkészítése
     *
     * @param array $programs
     * @param int $channelId
     * @param ProgramRepository $programRepository
     * @param AgeLimitRepository $ageLimitRepository
     * @param CalendarRepository $calendarRepository
     * @return void
     * @throws \Doctrine\DBAL\Exception
     */
    private function prepareSaveProgram(array              $programs,
                                        int                $channelId,
                                        ProgramRepository  $programRepository,
                                        AgeLimitRepository $ageLimitRepository,
                                        CalendarRepository $calendarRepository): void {

        foreach($programs as $program) {
            $startCalendarId = $calendarRepository->searchDateId(substr($program['start_datetime'], 0, 10));
            $finishCalendarId = $calendarRepository->searchDateId(substr($program['end_datetime'], 0, 10));
            $programExist = $programRepository->checkProgramExist($channelId, $startCalendarId, substr($program['start_datetime'], 11, 19), $program['title']);

            if(!$programExist) {
                $savedProgramData = array(
                    'channel_id' => $channelId,
                    'age_id' => $ageLimitRepository->searchAgeLimitId($program['restriction']['age_limit']),
                    'title' => $program['title'],
                    'start_calendar_id' => $startCalendarId,
                    'start' => $program['start_time'],
                    'finish_calendar_id' => $finishCalendarId,
                    'finish' => $program['end_time'],
                    'description' => $program['short_description']
                );

                $programRepository->createProgram($savedProgramData);
            }
        }
    }
}