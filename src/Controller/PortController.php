<?php

namespace App\Controller;

use App\Constant\Project;
use App\Lib\ChannelHandler;
use App\Repository\ProgramRepository;
use App\Repository\ChannelRepository;
use App\Repository\AgeLimitRepository;
use App\Repository\CalendarRepository;
use Exception;
use App\Lib\HttpClientHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;


/**
 * Class PortController
 */
class PortController extends AbstractController {

    /**
     * Műsor kilistázása
     *
     * @param Request $request
     * @param ChannelHandler $channelHandler
     * @param ChannelRepository $channelRepository
     * @param ProgramRepository $programRepository
     * @param CalendarRepository $calendarRepository
     * @return Response
     */
    #[Route('/list/menu', name: 'listProgram', methods: ['GET', 'POST'])]
    public function listPrograms(Request            $request,
                                 ChannelHandler     $channelHandler,
                                 ChannelRepository  $channelRepository,
                                 ProgramRepository  $programRepository,
                                 CalendarRepository $calendarRepository): Response {

        $channels = array();
        $programs = array();
        $programDates = array();

        try {
            $channelName = ($request->get('channelName') === NULL) ? array() : $request->get('channelName');
            $startDate = ($request->get('startDate') === NULL) ? "" : $request->get('startDate');

            $formData = $channelHandler->formValidation($channelName, $startDate, $calendarRepository);

            $channels = $channelRepository->listChannels();
            $programDates = $channelRepository->listProgramDates();
            $programs = $programRepository->listPrograms($formData);

            if(count($programs) === 0) {
                throw new Exception(Project::NOT_RESULT_MSG, Response::HTTP_NO_CONTENT);
            }
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('channel.html.twig', [
            'channelForm' => $channels,
            'dateForm' => $programDates,
            'data' => $programs
        ]);
    }

    /**
     * Műsor frissítése
     *
     * @param Request $request
     * @param ChannelHandler $channelHandler
     * @param HttpClientHandler $httpClientHandler
     * @param ChannelRepository $channelRepository
     * @param ProgramRepository $programRepository
     * @param AgeLimitRepository $ageLimitRepository
     * @param CalendarRepository $calendarRepository
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/update/menu', name: 'updateMenu', methods: ['GET', 'POST'])]
    public function updateProgram(Request            $request,
                                  ChannelHandler     $channelHandler,
                                  HttpClientHandler  $httpClientHandler,
                                  ChannelRepository  $channelRepository,
                                  ProgramRepository  $programRepository,
                                  AgeLimitRepository $ageLimitRepository,
                                  CalendarRepository $calendarRepository): Response {

        $channelsForForm = array();

        try {
            $channelName = ($request->get('channelName') === NULL) ? array() : $request->get('channelName');
            $date = ($request->get('startDate') === NULL) ? date('Y-m-d') : $request->get('startDate');

            $formData = $channelHandler->formValidation($channelName, $date, $calendarRepository);

            $channelNumber = $channelRepository->countChannel();
            if($channelNumber === 0) {
                $channelHandler->updateChannel($httpClientHandler, $channelRepository);
            }

            $channelsForForm = $channelRepository->listChannels();
            $channels = $channelRepository->searchChannelById($formData['channelList']);
            $channelHandler->programUpdating($channels, $date, $httpClientHandler, $programRepository, $ageLimitRepository, $calendarRepository);

            $this->addFlash('success', Project::CHANNEL_UPDATE_SUCCESS_MSG);
        } catch (Exception $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->render('updateChannel.html.twig', ['form' => $channelsForForm]);
    }
}